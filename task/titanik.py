import pandas as pd
'''pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1200)'''


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    df = get_titatic_dataframe()
    #print("After read df\n",df.head())

    # You can use the str.contains() method in pandas to check if a string column value contains one of the strings from a list.
    # List of strings to check against
    title_list = ["Mr.", "Mrs.", "Miss."]

    # Define a function to find the matching string from string_list
    def find_matching_string(text):
        for item in title_list:
            if item in text:
                return item
        return None

    # Use apply() to apply the function to each row in the DataFrame
    df['Title'] = df['Name'].apply(find_matching_string)

    #print("After make Title column\n", df)

    results = []

    for title in title_list:
        title_df = df[df['Title'] == title]

        median_age = title_df['Age'].median()

        median_age = round(median_age)

        missing_values = title_df['Age'].isnull().sum()

        results.append((title, missing_values, median_age))

    return results

#print(get_filled())